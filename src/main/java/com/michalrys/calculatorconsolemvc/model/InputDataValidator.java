package com.michalrys.calculatorconsolemvc.model;

public interface InputDataValidator {
    boolean canBeParsedToDouble(String value);
}
