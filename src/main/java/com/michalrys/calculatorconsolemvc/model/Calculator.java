package com.michalrys.calculatorconsolemvc.model;

public interface Calculator {
    void add(double a, double b);

    void add();

    double getResult();

    void setValueA(String a);

    void setValueB(String b);

    boolean getValueAWrongInput();

    boolean getValueBWrongInput();


    double getValueA();

    double getValueB();
}
