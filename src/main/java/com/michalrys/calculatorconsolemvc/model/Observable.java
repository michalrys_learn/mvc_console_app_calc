package com.michalrys.calculatorconsolemvc.model;

import com.michalrys.calculatorconsolemvc.MVC;

public interface Observable {
    void addView(MVC.View view);

    void deleteView(MVC.View view);

    void notifyAllViews();
}
