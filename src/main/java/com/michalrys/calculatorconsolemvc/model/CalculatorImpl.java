package com.michalrys.calculatorconsolemvc.model;

import com.michalrys.calculatorconsolemvc.MVC;

import java.util.ArrayList;
import java.util.List;

public class CalculatorImpl implements Calculator, Observable {
    private List<MVC.View> views = new ArrayList<>();
    private InputDataValidator validator = new SimpleValidator();
    private double result;
    private double valueA;
    private boolean valueAWrongInput = false;
    private boolean valueBWrongInput = false;
    private double valueB;

    @Override
    public void add(double a, double b) {
        result = a + b;
        notifyAllViews();
    }

    @Override
    public void add() {
        result = 0;
        result = valueA + valueB;
        notifyAllViews();
    }

    @Override
    public double getResult() {
        return result;
    }

    @Override
    public void setValueA(String a) {
        valueAWrongInput = false;
        if (validator.canBeParsedToDouble(a)) {
            valueA = Double.parseDouble(a);
        } else {
            valueAWrongInput = true;
        }
    }

    @Override
    public void setValueB(String b) {
        valueBWrongInput = false;
        if (validator.canBeParsedToDouble(b)) {
            valueB = Double.parseDouble(b);
        } else {
            valueBWrongInput = true;
        }
    }

    @Override
    public boolean getValueAWrongInput() {
        return valueAWrongInput;
    }

    @Override
    public boolean getValueBWrongInput() {
        return valueBWrongInput;
    }

    @Override
    public double getValueA() {
        return valueA;
    }

    @Override
    public double getValueB() {
        return valueB;
    }

    @Override
    public void addView(MVC.View view) {
        if (!views.contains(view)) {
            views.add(view);
        }
    }

    @Override
    public void deleteView(MVC.View view) {
        if (views.contains(view)) {
            views.remove(view);
        }
    }

    @Override
    public void notifyAllViews() {
        for (MVC.View view : views) {
            view.update();
        }
    }
}
