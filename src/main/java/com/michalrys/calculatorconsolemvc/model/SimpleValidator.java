package com.michalrys.calculatorconsolemvc.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleValidator implements InputDataValidator {
    @Override
    public boolean canBeParsedToDouble(String value) {
        Pattern pattern = Pattern.compile("[0-9]*|[0-9]*\\.[0-9]{1,}|-[0-9]*|-[0-9]*\\.[0-9]{1,}");
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }
}
