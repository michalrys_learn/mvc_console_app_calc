package com.michalrys.calculatorconsolemvc;

public interface MVC {
    interface Controller {
        void addValues(double a, double b);

        void add();

        void startMainLoopOfApplication();

        void readOption(String userInput);

        void readValue(String value, String valueIndex);
    }

    interface View {
        void update();

        void printResult();

        void addValues(double a, double b);

        void startApplication();

        void printMenu();

        void readValue(String valueIndex);

        void closeApplication();

        void readOptionFromUser();

        void wrongInputData();
    }
}
