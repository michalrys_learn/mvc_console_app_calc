package com.michalrys.calculatorconsolemvc.view;

import com.michalrys.calculatorconsolemvc.MVC;
import com.michalrys.calculatorconsolemvc.controller.CalculatorController;
import com.michalrys.calculatorconsolemvc.model.Calculator;
import com.michalrys.calculatorconsolemvc.model.Observable;

import java.util.Scanner;

public class SimpleConsoleView implements MVC.View {
    private Calculator model;
    private CalculatorController controller;
    private double result;
    private final Scanner SCANNER = new Scanner(System.in);

    public SimpleConsoleView(Calculator model) {
        this.model = model;
        ((Observable) model).addView(this);
        controller = new CalculatorController(model, this);

        startApplication();
    }

    @Override
    public void update() {
        result = model.getResult();
        printResult();
    }

    @Override
    public void printResult() {
        System.out.println("Result = " + result);
    }

    @Override
    public void addValues(double a, double b) {
        controller.addValues(a, b);
    }

    @Override
    public void startApplication() {
        controller.startMainLoopOfApplication();
    }

    @Override
    public void printMenu() {
        System.out.println("+-----------------------------------------+");
        System.out.println("| Calculator menu                         |");
        System.out.println("|           + : add two values.           |");
        System.out.println("|  other sign : exit application.         |");
        System.out.println("+ - - - - - - - - - - - - - - - - - - - - +");
    }

    @Override
    public void readOptionFromUser() {
        String userInput = SCANNER.nextLine();
        controller.readOption(userInput);
    }

    @Override
    public void readValue(String valueIndex) {
        System.out.print("Value " + valueIndex + " = ");
        String value1 = SCANNER.nextLine();
        controller.readValue(value1, valueIndex);
    }

    @Override
    public void wrongInputData() {
        System.out.println("Given input data is wrong. Input value must be a double precision numbers, like: -3.14, 7.");
    }

    @Override
    public void closeApplication() {
        System.out.println("Goodbye. Have a nice day.");
        System.exit(0);
    }
}