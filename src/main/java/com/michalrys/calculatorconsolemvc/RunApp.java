package com.michalrys.calculatorconsolemvc;

import com.michalrys.calculatorconsolemvc.model.Calculator;
import com.michalrys.calculatorconsolemvc.model.CalculatorImpl;
import com.michalrys.calculatorconsolemvc.view.SimpleConsoleView;

public class RunApp {
    public static void main(String[] args) {
        Calculator calculator = new CalculatorImpl();
        MVC.View view = new SimpleConsoleView(calculator);
    }
}
