package com.michalrys.calculatorconsolemvc.controller;

import com.michalrys.calculatorconsolemvc.MVC;
import com.michalrys.calculatorconsolemvc.model.Calculator;

public class CalculatorController implements MVC.Controller {
    private Calculator model;
    private MVC.View view;

    public CalculatorController(Calculator calculator, MVC.View view) {
        this.model = calculator;
        this.view = view;
    }

    @Override
    public void addValues(double a, double b) {
        model.add(a, b);
    }

    @Override
    public void add() {
        model.add();
    }

    @Override
    public void startMainLoopOfApplication() {
        while (true) {
            view.printMenu();
            view.readOptionFromUser();
        }
    }

    @Override
    public void readOption(String userInput) {
        switch (userInput) {
            case "+" : {
                view.readValue("1");
                view.readValue("2");

                if (model.getValueAWrongInput() || model.getValueBWrongInput()) {
                    view.wrongInputData();
                    break;
                } else {
                    add();
                    break;
                }
            }
            default: {
                view.closeApplication();
                break;
            }
        }
    }

    @Override
    public void readValue(String value, String valueIndex) {
        if (valueIndex.equals("1")) {
            model.setValueA(value);
        } else if (valueIndex.equals("2")) {
            model.setValueB(value);
        }
    }
}
